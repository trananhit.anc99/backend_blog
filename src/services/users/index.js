const register = require('../../models/register.model');
const hotel = require('../../models/hotel.model');
const booking = require('../../models/booking.model');
const  findUserByUsername = async (username) => {
    const findUsername =  register.find({username})
    findUsername
        .then(result => result)
        .catch(err => err)
    return findUsername;
}
const findUserById = async (id) => {
    const userById =  register.find({id})
    userById
        .then(result => result)
        .catch(err => err)
    return userById;
}
const getAllHotel = async () => {
    const allInfoHotel =  hotel.find({})
    allInfoHotel
        .then(result => result)
        .catch(err => err)
    return allInfoHotel;
}
const getAllBookingHotel = async () => {
    const allBookHotel =  booking.find({})
    allBookHotel
        .then(result => result)
        .catch(err => err)
    return allBookHotel;
}

const findPostById = async (id) => {
    const postById =  hotel.find({_id: id})
    postById
        .then(result => result)
        .catch(err => err)
    return postById;
}





module.exports = {
    findUserByUsername,
    findUserById,
    getAllHotel,
    findPostById,
    getAllBookingHotel
}