const mongoose = require('mongoose');
const {logger} = require('../../config/winston');
const DB =  (db) => {
    const connect = () => {
        mongoose
            .connect(
                db,
                {useNewUrlParser: true, useUnifiedTopology: true}
            )
            .then(() => logger.info(`DB Connected URL success`))
            .catch((err) => logger.error(`DB connect err: ${err}`))
    }
    connect();
    mongoose.connection.on('disconnected', connect)
}


module.exports = DB;