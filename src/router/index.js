const express = require('express');
const router = express.Router();
const {register} = require('../function/register/resgister.handle')
const {login, test} = require('../function/login/login.handle');
const { getInfoHotel, postInfoHotel, updateInfoHotel, deleteInfoHotel, getHotelById, bookingHotel, getBookingHotel  } = require('../function/hotel');
const { verifyToken, verifyTokenIsAdmin } = require('../function/auth/verifyToken');
router.post('/register', register);
router.post('/login', login);
router.get('/postHotel',verifyToken, getInfoHotel);
router.get('/bookingHotel',verifyTokenIsAdmin, getInfoHotel);
router.post('/postHotel', verifyTokenIsAdmin, postInfoHotel);
router.put('/postHotel/:id', verifyTokenIsAdmin, updateInfoHotel);
router.delete('/postHotel/:id', verifyTokenIsAdmin, deleteInfoHotel);
router.post('/bookingHotel', verifyToken, bookingHotel);
router.get('/getAllBooking', verifyTokenIsAdmin, getBookingHotel);
router.get('/getHotelById/:id', verifyTokenIsAdmin, getHotelById);


module.exports = router;