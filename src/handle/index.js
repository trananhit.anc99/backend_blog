const bcrypt = require('bcrypt');
const createError = require('http-errors');
module.exports = {
    handleErr: (codeStatus, customErr) => {
        return createError(codeStatus, `${customErr}`);
    }
};