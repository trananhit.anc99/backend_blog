const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const BookingHotel = new Schema({
    nameHotel: String,
    username: String,
    numberHotel: Number,
    publishedAt: { type: Date, default: Date.now },
});
module.exports = mongoose.model('BookingHotel', BookingHotel);