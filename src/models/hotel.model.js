const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * @param { String } name
 * @param { String } username
 */
const InfoHotel = new Schema({
    nameHotel:   String,
    descriptionsHotel:   String,
    avatar: String,
    numberHotel: Number,
    publishedAt: { type: Date, default: Date.now },
});
module.exports = mongoose.model('infoHotel', InfoHotel);