const mongoose = require('mongoose');
const Schema = mongoose.Schema;
/**
 * @param { String } name
 * @param { String } username
 */
const Register = new Schema({
    username:   String,
    passwd:   String,
    isRoleAdmin: { type: String, default: 'user' },
    publishedAt: { type: Date, default: Date.now },
});
module.exports = mongoose.model('registers', Register);