const bcrypt = require('bcrypt');
const { generateToken } = require('../auth')

const { findUserByUsername } = require('../../services/users')
const { handleErr } = require('../../handle')
const comparePassword = async (passwd, passwordHash) => {
    const match = await bcrypt.compare(passwd, passwordHash);
    return match;
}

module.exports = {
    login: async (req, res, next) => {
        const { username, passwd } = req.body;
        const findUsername = await findUserByUsername(username);
        const passwdNotMatch =  handleErr(401, ` Password does not match`)
        if(findUsername.length !== 1) {
            const usernameNotFound =  handleErr(401, `${username} - Username not found`)
            const {name, statusCode, message} = usernameNotFound
            return res
                .status(401)
                .json({name, statusCode, message});
        }
        const passwdHash = findUsername[0].passwd;
        const isMatch = await comparePassword(passwd, passwdHash);
        if(!isMatch) {
            const {name, statusCode, message} = passwdNotMatch
            return res
                .status(401)
                .json({name, statusCode, message});
        }
        const payload = {
            id: findUsername[0].id,
            username: findUsername[0].username,
            role: findUsername[0].isRoleAdmin,
        }
        const statusToken = await generateToken(payload);
        return res
            .status(200)
            .cookie('bearer', statusToken, {
            httpOnly: true,
            maxAge: 60000000,
            })
            .json(statusToken);
    },
    test: (req, res, next) => {
        res.send('ok')
    }

}