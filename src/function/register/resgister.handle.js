const bcrypt = require('bcrypt');
const registers = require('../../models/register.model');
const { findUserByUsername } = require('../../services/users')
const { handleErr } = require('../../handle')
// const {NotFound} = require('../../handle')
const hashPassword = async (passwd, saltRounds = 10) => {
    const passwdHash = new Promise((resolve, reject) => {
        bcrypt.hash(passwd, saltRounds, (err, passwdHashed) => {
            if(err) reject(err);
            resolve(passwdHashed);
        });
    }) 
    return passwdHash;
}
module.exports = {
    register: async (req, res, next) => {
        const {username, passwd } = req.body;
        const passwdHashed = await hashPassword(passwd);
        const findUsername = await findUserByUsername(username);
        if (findUsername.length !== 0) {
            const errUsername = await handleErr(401, `${username} - Username has already been taken`);
            const {name, statusCode, message} = errUsername
            return res.json({name, statusCode, message});
        }
        try {
            const account = new registers({username, passwd: passwdHashed});
            await account.save();
            res.send('ok')
        } catch (error) {
            res.json(await handleErr(500, `${error} - Internal Server Error`));
        }
    }
}