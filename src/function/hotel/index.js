const infoHotel = require('../../models/hotel.model');
const { getAllHotel, findPostById, getAllBookingHotel } = require('../../services/users');
const { verifyToken } = require('../auth')
const hotel = require('../../models/hotel.model');
const bookHotel = require('../../models/booking.model');
const { handleErr } = require('../../handle')
module.exports = {
    postInfoHotel: async (req, res, next) => {
        // nameHotel:   String,
        // descriptionsHotel:   String,
        // avatar: String,
        // numberHotel: Number,
        const {nameHotel, descriptionsHotel, avatar, numberHotel } = req.body;
        try {
            const saveInfoHotel = new infoHotel({nameHotel, descriptionsHotel, avatar, numberHotel});
            await saveInfoHotel.save();
            res.send('ok')
        } catch (error) {
            res
                .status(500)
                .json(await handleErr(500, `${error} - Internal Server Error`));
        }
    },
    getInfoHotel: async (req, res, next) => {
        
        try {
            const allInfoHotel = await getAllHotel();
            res
                .status(200)
                .send(allInfoHotel);
        } catch (error) {
            res
                .status(500)
                .json(await handleErr(500, `${error} - Internal Server Error`));
        }
    },
    updateInfoHotel: async (req, res, next) => {
        const id = req.params.id
        const {nameHotel, descriptionsHotel, avatar, numberHotel } = req.body;
        try {
            await hotel.findOneAndUpdate({_id: id}, {nameHotel, descriptionsHotel, avatar, numberHotel });
            res.send('ok')
        } catch (error) {
            res
                .status(500)
                .json(await handleErr(500, `${error} - Internal Server Error`));
        }
    },
    deleteInfoHotel: async (req, res, next) => {
        const id = req.params.id
        try {
            await hotel.deleteOne({_id: id}, (err) => {
                if(err) {return res.json({err})}
                res
                    .status(200)
                    .json({'mess': 'Delete success'})
            });
        } catch (error) {
            res
                .status(500)
                .json(await handleErr(500, `${error} - Internal Server Error`));
        }
    },
    bookingHotel: async (req, res, next) => {
        const {bearer} = req.headers;
        const { id } = req.body;
        const deToken = await verifyToken(bearer);
        const findPost = await findPostById(id);
        if(findPost.length === 0) {
            return res.send('Ko co info')
        }
        try {
            const username = deToken.username
            const { nameHotel, numberHotel } = findPost[0];
            // nameHotel: String,
            // username: String,
            // numberHotel: Number,
            const saveUserBooking = new bookHotel({nameHotel, username, numberHotel});
            await saveUserBooking.save();
            res
                .status(200)
                .json({'mess': 'booking success'})
        } catch (error) {
            res
                .status(500)
                .json(await handleErr(500, `${error} - Internal Server Error`));
        }
    },
    getBookingHotel: async (req, res, next) => {
        try {
            const saveUserBooking = await getAllBookingHotel();
            res
                .status(200)
                .send(saveUserBooking);
        } catch (error) {
            res
                .status(500)
                .json(await handleErr(500, `${error} - Internal Server Error`));
        }
    },
    getHotelById: async (req, res, next) => {
        const id = req.params.id
        try {
            const data = await hotel.findOne({_id: id});
            console.log(data)
            res.send(data);
        } catch (error) {
            res
                .status(500)
                .json(await handleErr(500, `${error} - Internal Server Error`));
        }
    },
}