const { handleErr } = require('../../handle');
const { verifyToken } = require('../auth');
const { findUserByUsername } = require('../../services/users')
module.exports = {
    verifyToken: async (req, res, next) => {
        const {bearer} = req.headers;
        if(!bearer) return res.json(handleErr(401, 'Access Denied'));
        try {
            const deToken = await verifyToken(bearer);
            req.username = deToken;
            next();
        } catch (error) {
            res.json(handleErr(400, 'Invalid token'));
        }
    },
    verifyTokenIsAdmin: async (req, res, next) => {
        const {bearer} = req.headers;
        if(!bearer) return res.json(handleErr(401, 'Access Denied'));        
        try {
            const deToken = await verifyToken(bearer);
            const {username}= deToken;
            const isRole = await findUserByUsername(username);
            if(isRole[0].isRoleAdmin !== 'Admin') return res.json(handleErr(402, 'Is not admin'));
            next();
        } catch (error) {
            res.json(handleErr(400, 'Invalid token'));
        }
    }
}