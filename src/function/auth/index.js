const jwt = require('jsonwebtoken');
const privateKey = 'asdasdasd';
module.exports = {
    generateToken : async (payload) => new Promise((resolve, reject) => {
        jwt.sign(payload, privateKey, (err, token) => {
            if(err) return reject(err);
            return resolve(token);
        })
    }),
    verifyToken: async (token) => {
        const deToken = jwt.verify(token, privateKey)
        return deToken;
    }
    
}
