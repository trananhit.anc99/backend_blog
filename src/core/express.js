const express =  require('express');
const morgan = require('morgan');
const chalk = require('chalk')
const cors = require('cors');
const bodyParser = require('body-parser')
const routers = require('../router');
// Import config winston
const {logger, morganOption} = require('../../config/winston');
const ServerNode = async () => {
    const app = express();
    const port = 8000
    // Cors
    app.use(cors());
    app.use(bodyParser.urlencoded({ extended: false }));
    app.use(bodyParser.json())
    // Morgan
    app.use(morgan('tiny',morganOption));
    app.get('/', (req, res) => res.send('Hello World!'))
    app.use('/v1', routers);
    app.listen(port, () => logger.info((chalk.bold.cyanBright.underline)(`Port run ${port}`)))
}


module.exports = ServerNode;