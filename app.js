const { urlMongodb } = require('./config/default');
const ServerNode = require('./src/core/express');
const DB = require('./src/database/mongoose');
const startServe = () => {
   DB(urlMongodb);
   ServerNode();
}

startServe();
