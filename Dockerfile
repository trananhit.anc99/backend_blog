FROM node:latest
WORKDIR /home/opt
RUN apt-get update
COPY ["package.json", "package-lock.json", "./"]
RUN npm install --only=production
COPY . .
EXPOSE 8000
CMD npm run start