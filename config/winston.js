const appRoot = require('app-root-path');
const winston =require('winston');


const options = {
    file: {
        level: 'info',
        filename: `${appRoot}/logs/app.log`,
        handleExceptions: true,
        json: true,
        maxsize: 5242880, // 5MB
        maxFiles: 5,
        colorize: true,
    },
    console: {
        level: 'debug',
        handleExceptions: true,
        json: false,
        colorize: true,
    },
    error: {
        level: 'error',
        handleExceptions: true,
        json: true,
        colorize: true,
    }
  };

const logger = winston.createLogger({
    transports: [
        new winston.transports.File(options.file),
        new winston.transports.Console(options.console)
    ],
    format: winston.format.combine(
        winston.format.colorize({ colors: {info: 'blue', error: 'red', debug: 'yellow'} }),
        winston.format.simple()
    )
});

const morganOption = {
    stream: {
        write (message) {
            logger.info(message.trim());
        },
    },
  };


module.exports = {
    logger, 
    morganOption
}